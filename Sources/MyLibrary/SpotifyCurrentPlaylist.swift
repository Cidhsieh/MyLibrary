//
//  SpotifyCurrentPlaylist.swift
//  atSoul
//
//  Created by cidhsieh on 2021/9/16.
//

import Foundation
public struct SpotifyCurrentPlaylist: Codable {

    let limit: Int?
    let href: String?

    let offset: Int?

    let total: Int?

    public struct Item: Codable {

        public struct Owner: Codable {

            let external_urls: ExternalUrls?

            let uri: String?
            let href: String?
            let id: String?
            let display_name: String?
            let type: String?
        
        }

        let owner: Owner?

        public struct Tracks: Codable {

            let total: Int?
            let href: String?
        
        }

        let tracks: Tracks?

        public struct ExternalUrls: Codable {

            let spotify: String?
        
        }

        let external_urls: ExternalUrls?

        let name: String?
        let href: String?
        let uri: String?
        let description: String?

        let type: String?
        let snapshot_id: String?
        let collaborative: Bool?
        let id: String?

        public struct Image: Codable {

            let height: Double?
            let url: String?
            let width: Double?
        
        }

        let images: [Image]?

        let `public`: Bool?
    }

    let items: [Item]?

}
