//
//  SpotifyPlayModel.swift
//  atSoul
//
//  Created by cidhsieh on 2021/9/27.
//

import Foundation

public struct SpotifyPlay: Codable {
    var context_uri: String
    var offset: Offset
    
    public struct Offset: Codable {
        var position: Int = 0
    }
}
