//
//  SpotifyProfileModel.swift
//  atSoul
//
//  Created by cidhsieh on 2021/9/29.
//

import Foundation

public struct SpotifyProfileModel: Codable {
    let country: String?
    let display_name: String?
    let email: String?
    let product: String?
    let images: [Image]?
    
    public struct Image: Codable {
        let height: Double?
        let url: String?
        let width: Double?
    }
}
