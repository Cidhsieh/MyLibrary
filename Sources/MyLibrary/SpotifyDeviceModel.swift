//
//  SpotifyDeviceModel.swift
//  atSoul
//
//  Created by cidhsieh on 2021/9/28.
//

import Foundation

public struct SpotifyDeviceModel: Codable {
    var devices: [Device]
    struct Device: Codable {
        var id: String
        var is_active: Bool
        var is_private_session: Bool
        var is_restricted: Bool
        var name: String
        var type: String
        var volume_percent: Int
    }
}
