import UIKit
import StoreKit
import Foundation
import SpotifyiOS


public class SpotifyManager: NSObject, ObservableObject {
    public static let shared = SpotifyManager()
    @Published public var isPaused: Bool = true
    @Published public var isShowAppleStore: Bool = false
    @Published public var isConnected: Bool = false
    @Published public var playerState: SPTAppRemotePlayerState?
    @Published public var trackImage: UIImage?
    @Published public var profile: SpotifyProfileModel?
    @Published public var playList: SpotifyCurrentPlaylist?
    @Published public var playItem: SpotifyPlaylistItem?
    static let kAccessTokenKey = "access-token-key"
    private let redirectUri = URL(string:"atSoul://")!
    private let clientIdentifier = "a5cacb00d4ee426c98bf99eee8234d39"
    var trackIdentifier = ""
    var device: SpotifyDeviceModel?
    
    lazy var appRemote: SPTAppRemote = {
        let configuration = SPTConfiguration(clientID: self.clientIdentifier, redirectURL: self.redirectUri)
        let appRemote = SPTAppRemote(configuration: configuration, logLevel: .debug)
        appRemote.connectionParameters.accessToken = self.accessToken
        appRemote.delegate = self
        return appRemote
    }()
    
    var accessToken = UserDefaults.standard.string(forKey: kAccessTokenKey) {
        didSet {
            let defaults = UserDefaults.standard
            defaults.set(accessToken, forKey: SpotifyManager.kAccessTokenKey)
            print("\n\n ====== accessToken ======\n \(accessToken ?? "")\n\n")
        }
    }
    
    public func remoteLink() {
        if appRemote.authorizeAndPlayURI(trackIdentifier, asRadio: true, additionalScopes: ["playlist-read-private",
                                                                                            "playlist-read-collaborative",
                                                                                            "user-read-playback-state",
                                                                                            "user-modify-playback-state",
                                                                                            "user-follow-read",
                                                                                            "user-library-read",
                                                                                            "user-read-private",
                                                                                            "user-read-email"]) {
            
        } else {
            // Presents a StoreKit overlay
            isShowAppleStore = true
        }
    }
    
    public func getPlayerState() {
        appRemote.playerAPI?.getPlayerState { (result, error) -> Void in
            guard error == nil else { return }
            let playerState = result as! SPTAppRemotePlayerState
            self.isPaused = playerState.isPaused
            self.trackIdentifier = playerState.contextURI.absoluteString
        }
    }
    
    func playTrack() {
        if appRemote.isConnected {
            appRemote.playerAPI?.play(trackIdentifier, callback: defaultCallback)
        } else {
            remoteLink()
        }
    }
    
    func play(playListIndex: Int, selectIndex: Int = 0) {
        if device != nil {
            let encode = JSONEncoder()
            let data = try! encode.encode(SpotifyPlay(context_uri: playList?.items?[playListIndex].uri ?? "", offset: SpotifyPlay.Offset(position: selectIndex)))
            requstEncodeBody(httpMethod: "PUT", .startResume, "", body: data)
        } else {
            requstEncodeBody(.getAvailableDevice)
        }
    }
    
    func resumePause() {
        if appRemote.isConnected {
            if isPaused {
                appRemote.playerAPI?.resume(defaultCallback)
            } else {
                appRemote.playerAPI?.pause(defaultCallback)
            }
            isPaused.toggle()
        } else {
            remoteLink()
        }
    }
    
    func next() {
        appRemote.playerAPI?.skip(toNext: defaultCallback)
    }
    
    func previous() {
        appRemote.playerAPI?.skip(toPrevious: defaultCallback)
    }
    
    var defaultCallback: SPTAppRemoteCallback {
        get {
            return { result , error in
                if let error = error {
                    print(error.localizedDescription)
                }
            }
        }
    }
}


extension SpotifyManager: SPTAppRemoteDelegate, SPTAppRemotePlayerStateDelegate {
    
    public func appRemoteDidEstablishConnection(_ appRemote: SPTAppRemote) {
        print("connected")
        appRemote.playerAPI?.delegate = self
        appRemote.playerAPI?.subscribe(toPlayerState: defaultCallback)
        isConnected = true
        getPlayerState()
        requstEncodeBody(.profile)
        requstEncodeBody(.getAvailableDevice)
    }
    
    public func appRemote(_ appRemote: SPTAppRemote, didFailConnectionAttemptWithError error: Error?) {
        print("failed")
        isConnected = false
        playerState = nil
    }
    
    public func appRemote(_ appRemote: SPTAppRemote, didDisconnectWithError error: Error?) {
        print("disconnected")
        isConnected = false
        playerState = nil
    }
    
    public func playerStateDidChange(_ playerState: SPTAppRemotePlayerState) {
        self.playerState = playerState
        
        appRemote.imageAPI?.fetchImage(forItem: playerState.track, with: CGSize(width: 400, height: 400), callback: { (image, error) in
            guard error == nil else { return }
            self.trackImage = image as? UIImage
        })
    }
}


extension SpotifyManager {
    
    public enum APIType {
        case profile
        case userPlayList
        case playListItems
        case getAvailableDevice
        case startResume
    }
    
    public func requstEncodeBody(httpMethod: String = "GET", _ type: APIType, _ id: String = "", body: Data? = nil) {
        var url: URL!
        
        switch type {
        case .profile:
            url = URL(string: "https://api.spotify.com/v1/me")
        case .userPlayList:
            url = URL(string: "https://api.spotify.com/v1/me/playlists")!
        case .playListItems:
            url = URL(string: "https://api.spotify.com/v1/playlists/\(id)/tracks")!
        case .getAvailableDevice:
            url = URL(string: "https://api.spotify.com/v1/me/player/devices")!
        case .startResume:
            url = URL(string: "https://api.spotify.com/v1/me/player/play?device_id=\(device?.devices.filter { $0.type == "Smartphone" }.first?.id ?? "")")!
        }
        var request = URLRequest(url: url)
        request.httpMethod = httpMethod
        request.setValue("application/json", forHTTPHeaderField: "Accept")
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("Bearer \(UserDefaults.standard.string(forKey: SpotifyManager.kAccessTokenKey) ?? "")", forHTTPHeaderField: "Authorization")
        request.httpBody = body
        fetchData(type, request)
    }
    
    private func fetchData(_ type: APIType, _ request: URLRequest) {
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                print(error?.localizedDescription ?? "")
            } else {
                guard let data = data else{return}
                let decoder = JSONDecoder()
                DispatchQueue.main.async {
                    do {
                        switch type {
                        case .profile:
                            self.profile = try decoder.decode(SpotifyProfileModel.self, from: data)
                        case .userPlayList:
                            self.playList = try decoder.decode(SpotifyCurrentPlaylist.self, from: data)
                        case .playListItems:
                            self.playItem = try decoder.decode(SpotifyPlaylistItem.self, from: data)
                        case .getAvailableDevice:
                            self.device = try decoder.decode(SpotifyDeviceModel.self, from: data)
                        default:
                            break
                        }
                    } catch let error {
                        print(error.localizedDescription)
                    }
                }
            }
        }
        task.resume()
    }
}
