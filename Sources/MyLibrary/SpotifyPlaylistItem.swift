//
//  SpotifyPlaylistItem.swift
//  atSoul
//
//  Created by cidhsieh on 2021/9/16.
//

import Foundation

public struct SpotifyPlaylistItem: Codable {

    let limit: Int?
    let href: String?

    let offset: Int?

    let total: Int?

    public struct Item: Codable {
        public struct ExternalUrls: Codable {

            let spotify: String?
        
        }

        public struct Track: Codable {

            public struct Album: Codable {

                let external_urls: ExternalUrls?

                let name: String?
                let total_tracks: Int?
                let uri: String?
                let href: String?
                let type: String?

                public struct Artist: Codable {

                    let external_urls: ExternalUrls?

                    let name: String?
                    let href: String?
                    let uri: String?
                    let id: String?
                    let type: String?
                }

                let artists: [Artist]?

                let release_date: String?
                let id: String?
                let available_markets: [String]?
                let release_date_precision: String?

                public struct Image: Codable {

                    let height: Double?
                    let url: String?
                    let width: Double?
                
                }

                let images: [Image]?

                let album_type: String?

            }

            let album: Album?

            public struct ExternalIds: Codable {

                let isrc: String?
            
            }

            let external_ids: ExternalIds?

            let external_urls: ExternalUrls?

            let name: String?
            let href: String?
            let uri: String?
            let duration_ms: Int?
            let track: Bool?
            let preview_url: String?
            let explicit: Bool?
            let type: String?
            let episode: Bool?
            let track_number: Int?

            public struct Artist: Codable {

                let external_urls: ExternalUrls?

                let name: String?
                let href: String?
                let uri: String?
                let id: String?
                let type: String?

            }

            let artists: [Artist]?

            let is_local: Bool?
            let popularity: Int?
            let id: String?
            let disc_number: Int?
            let available_markets: [String]?

        }

        let track: Track?

        public struct AddedBy: Codable {

            let external_urls: ExternalUrls?

            let href: String?
            let id: String?
            let type: String?
            let uri: String?
        }

        let added_by: AddedBy?

        public struct VideoThumbnail: Codable {

        
        }

        let video_thumbnail: VideoThumbnail?

        let is_local: Bool?

        let added_at: String?
    
    }

    let items: [Item]?

}
